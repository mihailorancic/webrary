<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<head>
<title>Webrary</title>

<spring:url value="/resources/core/css/hello.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
</head>

<spring:url value="/register" var="registerUrl" />
<spring:url value="/j_spring_security_check" var="checkUrl" />

<nav class="navbar navbar-inverse ">
    
    <div class="container">
        <div id="navbar">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="${registerUrl}">Register</a></li>
            </ul>
        </div>
    </div>
</nav>
<c:if test="${not empty param.error}">
    <font color="red">
        Login error. <br />
        Reason : ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
    </font>
</c:if>
    <form class="form-horizontal" method="post" action="${checkUrl}">
    
    <div class="form-group">
            <label class="col-sm-2 control-label">Username</label>
            <div class="col-sm-10">
                    <input type="text" name="j_username" placeholder="please enter username" />
            </div>
    </div>

    <div class="form-group">
            <label class="col-sm-2 control-label">Password</label>
            <div class="col-sm-10">
                <input type="password" name="j_password" placeholder="please enter password" /> <br><br>
                <button type="submit" class="btn-lg btn-primary">Login</button>
            </div>
    </div>    
    
</form>
<div class="container">
    <hr>
    <footer>
        <p>Welcome to Webrary</p>
    </footer>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<spring:url value="/resources/core/js/hello.js" var="coreJs" />
<spring:url value="/resources/core/js/bootstrap.min.js"
	var="bootstrapJs" />

<script src="${coreJs}"></script>
<script src="${bootstrapJs}"></script>
</body>
</html>
<%@ page session="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="../fragments/header.jsp" />

<div class="container">

	<c:choose>
		<c:when test="${bookForm['new']}">
			<h1>Add Book</h1>
		</c:when>
		<c:otherwise>
			<h1>Update Book</h1>
		</c:otherwise>
	</c:choose>
	<br />

	<spring:url value="/books" var="bookActionUrl" />

	<form:form class="form-horizontal" method="post" modelAttribute="bookForm" action="${bookActionUrl}">

		<form:hidden path="id" />

		<spring:bind path="title">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label">Title</label>
				<div class="col-sm-10">
					<form:input path="title" type="text" class="form-control " id="title" placeholder="please enter title" />
					<form:errors path="title" class="control-label" />
				</div>
			</div>
		</spring:bind>

		<spring:bind path="author">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label">Author</label>
				<div class="col-sm-10">
					<form:input path="author" class="form-control" id="author" placeholder="please enter author name" />
					<form:errors path="author" class="control-label" />
				</div>
			</div>
		</spring:bind>

		<spring:bind path="isbn">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label">ISBN</label>
				<div class="col-sm-10">
					<form:input path="isbn" class="form-control" id="isbn" placeholder="please enter ISBN number" />
					<form:errors path="isbn" class="control-label" />
				</div>
			</div>
		</spring:bind>
                
                <form:hidden path="availability" />

		<!-- Custom Script, Spring map to model via 'name' attribute
		<div class="form-group">
			<label class="col-sm-2 control-label">Number</label>
			<div class="col-sm-10">

				<c:forEach items="${numberList}" var="obj">
					<div class="radio">
						<label> 
							<input type="radio" name="number" value="${obj}">${obj}
						</label>
					</div>
				</c:forEach>
			</div>
		</div>
 		-->

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<c:choose>
					<c:when test="${bookForm['new']}">
						<button type="submit" class="btn-lg btn-primary pull-right">Add</button>
					</c:when>
					<c:otherwise>
						<button type="submit" class="btn-lg btn-primary pull-right">Update</button>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</form:form>

</div>

<jsp:include page="../fragments/footer.jsp" />

</body>
</html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<head>
<title>Webrary</title>

<spring:url value="/resources/core/css/hello.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
</head>

<spring:url value="/books/list" var="urlHome" />
<spring:url value="/books/add" var="urlAddBook" />
<spring:url value="/logout" var="logoutUrl" />
<spring:url value="/users/list" var="usersUrl" />

<nav class="navbar navbar-inverse ">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="${urlHome}">Webrary Home</a>
		</div>
		<div id="navbar">
			<ul class="nav navbar-nav navbar-right">
                            <sec:authorize ifAnyGranted="ROLE_ADMIN">
				<li class="active"><a href="${urlAddBook}">Add Book</a></li>
                                <li class="active"><a href="${usersUrl}">Users</a></li>
                            </sec:authorize>
                                <li class="active"><a href="${logoutUrl}">Logout</a></li>
			</ul>
		</div>
	</div>
</nav>
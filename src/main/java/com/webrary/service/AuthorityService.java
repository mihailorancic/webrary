package com.webrary.service;

import com.webrary.model.Authority;
import com.webrary.model.AuthorityDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("authorityService")
public class AuthorityService {
    
    AuthorityDao authorityDao;
    
    @Autowired
    public void setAuthorityDao(AuthorityDao authorityDao) {
        this.authorityDao = authorityDao;
    }
        
    public void setAuthority(Authority authority){
        authorityDao.setAuthority(authority);
    }
    
    public Authority getAuthorityForUser (String username){
       Authority authority = authorityDao.getAuthorityForUser(username);
        return authority;
    }
}

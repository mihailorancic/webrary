package com.webrary.service;

import com.webrary.model.Book;
import com.webrary.model.BookDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("bookService")
public class BookService {
    
    BookDao bookDao;
    
    @Autowired
    public void setBookDao(BookDao bookDao) {
    this.bookDao = bookDao;
    }
    
    public Book getBookById(int id){
        return bookDao.getBookById(id);
    }
    
    public void addOrUpdate (Book book){
        if (book.isNew()) {
                bookDao.addBook(book);
        } else {
                bookDao.updateBook(book);
        }
    }
    
    public List<Book> listBooks(){
        return bookDao.listBooks();
    }
    
    public void removeBook(int id){
        bookDao.removeBook(id);
    }
}

package com.webrary.service;

import com.webrary.model.User;
import com.webrary.model.UserDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserService {
    
    UserDao userDao;
    
    @Autowired
    public void setUserDao(UserDao userDao) {
    this.userDao = userDao;
    }
    
    public User getUserByUsername(String username){
        return userDao.getUserByUsername(username);
    }
    
    public void addUser (User user){
        userDao.addUser(user);
    }
    
    public List<User> listUsers(){
        return userDao.listUsers();
    }
    
    public String setHashedPassword (User user, String password){
        user.setPassword(password);
        return password;
    } 
    /*
    public void removeBook(int id){
        bookDao.removeBook(id);
    }
    */
}

package com.webrary.model;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
public class AuthorityDao {
    
    @Autowired
    private SessionFactory sessionFactory;
	
    public void setSessionFactory(SessionFactory sf){
	this.sessionFactory = sf;
	}
    
    public void addAuthority(Authority authority) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(authority);
    }
    
    public void setAuthority(Authority authority){
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(authority);
    }
    
    public Authority getAuthorityForUser(String username){
        Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Authority.class)
                .add(Restrictions.eq("username", username));
        Authority authority = (Authority)criteria.uniqueResult();
        return authority;
    }
}

package com.webrary.model;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
public class UserDao {
    
    @Autowired
    private SessionFactory sessionFactory;
	
    public void setSessionFactory(SessionFactory sf){
	this.sessionFactory = sf;
	}
    
    public void addUser(User user) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(user);
        System.out.println("User added " + user.getUsername());
    }
    
    public User getUserByUsername(String username){
        Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(User.class)
                .add(Restrictions.eq("username", username));
        User user = (User)criteria.uniqueResult();
	return user;
    }
    
    public List<User> listUsers() {
        Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(User.class);
        List<User> userList = (List<User>)criteria.list();
	return userList;
        }
}

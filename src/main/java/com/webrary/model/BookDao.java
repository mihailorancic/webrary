package com.webrary.model;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
public class BookDao {
    
    @Autowired
    private SessionFactory sessionFactory;
	
    public void setSessionFactory(SessionFactory sf){
	this.sessionFactory = sf;
	}

    public void addBook(Book book) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(book);
        }

    public void updateBook(Book book) {
        Session session = this.sessionFactory.getCurrentSession();
	session.update(book);
    }

    public List<Book> listBooks() {
        Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Book.class);
        List<Book> bookList = (List<Book>)criteria.list();
	return bookList;
    }

    public Book getBookById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Book book = (Book) session.get(Book.class, id);
	return book;
    }

    public void removeBook(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Book book = (Book) session.get(Book.class, id);
        if(null != book){
                session.delete(book);
        }
    }
    
}

package com.webrary.controller;

import com.webrary.model.Book;
import com.webrary.service.BookService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class BookController {
    
    @Autowired
    BookService bookService;
    
    //show book list
    @RequestMapping("/books/list")
    public String list(ModelMap model){
        List<Book> allBooks = bookService.listBooks();
        model.addAttribute("allBooks", allBooks);
        return "books/list";
    }
    
    //show details for book by id
    @RequestMapping("/books/{id}")
    public String bookById(@PathVariable int id, ModelMap model){
        Book bookById = bookService.getBookById(id);
        model.addAttribute("bookById", bookById);
        return "books/show";
    }
    
    //show add book form
    @RequestMapping(value = "/books/add", method = RequestMethod.GET)
    public String showAddBookForm(Model model){
        
        Book book = new Book();
        book.setAvailability(true);
                
        model.addAttribute("bookForm", book);
        return "books/bookform";
    }
    
    //save or update book
    @RequestMapping(value = "/books", method = RequestMethod.POST)
    public String saveOrUpdateBook(@ModelAttribute("bookForm")@Valid Book book, BindingResult result, Model model, final RedirectAttributes redirectAttributes){
        
        if (result.hasErrors()) {
            return "books/bookform";
        } else {
            redirectAttributes.addFlashAttribute("css", "success");
            if(book.isNew()){
                redirectAttributes.addFlashAttribute("msg", "Book added successfully!");
            }else{
                redirectAttributes.addFlashAttribute("msg", "Book updated successfully!");
            }

            bookService.addOrUpdate(book);

            // POST/REDIRECT/GET
            return "redirect:/books/" + book.getId();

            // POST/FORWARD/GET
            // return "books/list";
        }
    }
    
    // delete book
    @RequestMapping(value = "/books/{id}/delete", method = RequestMethod.POST)
    public String deleteBook(@PathVariable("id") int id, final RedirectAttributes redirectAttributes) {
        bookService.removeBook(id);

        redirectAttributes.addFlashAttribute("css", "success");
        redirectAttributes.addFlashAttribute("msg", "Book is deleted!");

        return "redirect:/books/list";
    }
    
    // show update form
    @RequestMapping(value = "/books/{id}/update", method = RequestMethod.GET)
    public String showUpdateBookForm(@PathVariable("id") int id, Model model) {

        Book book = bookService.getBookById(id);
        model.addAttribute("bookForm", book);

        return "books/bookform";
    }
}

package com.webrary.controller;

import com.webrary.model.Authority;
import com.webrary.model.User;
import com.webrary.service.AuthorityService;
import com.webrary.service.UserService;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class UserController {
    
    @Autowired
    UserService userService;
    
    @Autowired
    AuthorityService authorityService;
    
    //show all users
    @RequestMapping("/users/list")
    public String userList(ModelMap model){
        List<User> allUsers = userService.listUsers();
        model.addAttribute("allUsers", allUsers);
        return "users/list";
    }
    
    //show user details
    @RequestMapping("/users/{username}")
    public String userByUsername(@PathVariable String username, ModelMap model){
        User userByUsername = userService.getUserByUsername(username);
        model.addAttribute("UserByUsername", userByUsername);
        Authority authorityForUser = authorityService.getAuthorityForUser(username);
        model.addAttribute("AuthorityForUser", authorityForUser);
        System.out.println("Username is: " + userByUsername.getUsername());
        System.out.println("Role is: " + authorityForUser.getAuthority());
        return "users/show";
    }
    
    //save user
    @RequestMapping(value = "/welcome", method = RequestMethod.POST)
    public String saveUser(@ModelAttribute("userForm")@Valid User user, BindingResult result, Model model, final RedirectAttributes redirectAttributes) throws NoSuchAlgorithmException{
        
        if (result.hasErrors()) {
            return "users/userform";
        } else {
            redirectAttributes.addFlashAttribute("css", "success");
            redirectAttributes.addFlashAttribute("msg", "User added successfully!");
            
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(user.getPassword().getBytes());
            
            byte byteData[] = md.digest();
            
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            userService.setHashedPassword(user, sb.toString());
            userService.addUser(user);
                        
            Authority authority = new Authority();
            authority.setUsername(user.getUsername());
            authority.setAuthority("ROLE_USER");
            authorityService.setAuthority(authority);

            // POST/REDIRECT/GET
            return "welcome";

            // POST/FORWARD/GET
            // return "books/list";
        }
    }
    
    //show registration form
    @RequestMapping(value = "/users/add", method = RequestMethod.GET)
    public String showAddUserForm(Model model) throws NoSuchAlgorithmException{
        
        User user = new User();
                        
        model.addAttribute("userForm", user);
        return "users/userform";
    }
}

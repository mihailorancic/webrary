package com.webrary.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WebraryController {
           
    /*@Autowired
    BookFormValidator bookFormValidator;
    
    @InitBinder("book")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(bookFormValidator);
    } */
    
    //show login page
    @RequestMapping(value ={"/login", "/"})
    public String logIn (Model model){
        return "login";
    }
    
    @RequestMapping(value = "/register")
    public String registerUser(Model model){
        return "redirect:/users/add";
    }
}

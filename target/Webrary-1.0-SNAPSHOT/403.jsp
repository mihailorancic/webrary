<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
    <title>Error Page</title>
</head>
<body>
    <spring:url value="/logout" var="logoutUrl" />
    <h1>Permission Denied</h1>
    <form method="POST" action="<c:url value="/logout"/>">
        <input type="submit" value="Logout" />
    </form>
</body>
</html>

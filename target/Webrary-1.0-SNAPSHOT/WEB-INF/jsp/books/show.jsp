<%@ page session="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="../fragments/header.jsp" />

<div class="container">
    
	<c:if test="${not empty msg}">
		<div class="alert alert-${css} alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<strong>${msg}</strong>
		</div>
	</c:if>

	<h1>Book Details</h1>
	<br />

	<div class="row">
		<label class="col-sm-2">ID</label>
		<div class="col-sm-10">${bookById.id}</div>
	</div>

	<div class="row">
		<label class="col-sm-2">Title</label>
		<div class="col-sm-10">${bookById.title}</div>
	</div>

	<div class="row">
		<label class="col-sm-2">Author</label>
		<div class="col-sm-10">${bookById.author}</div>
	</div>

	<div class="row">
		<label class="col-sm-2">ISBN</label>
		<div class="col-sm-10">${bookById.isbn}</div>
	</div>

	<div class="row">
		<label class="col-sm-2">Availability</label>
		<div class="col-sm-10">
                    <c:choose> 
                        <c:when test="${bookById.availability == true}">
                            <c:out value="available"/>
                        </c:when>
                        <c:otherwise>
                          <c:out value="unavailable"/>
                        </c:otherwise>
                    </c:choose></div>
	</div>
        <div class="row">
            <div style="margin-left: 15px">
            <spring:url value="/books/${bookById.id}/update" var="updateUrl" />
            <sec:authorize ifAnyGranted="ROLE_ADMIN">
            <button class="btn btn-primary" onclick="location.href='${updateUrl}'">Update</button>
            </sec:authorize>
            </div>
        </div>

</div>

<jsp:include page="../fragments/footer.jsp" />

</body>
</html>